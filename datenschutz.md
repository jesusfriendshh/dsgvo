Uns ist ein verantwortungs- und vertrauensvoller Umgang mit personenbezogenen Daten wichtig. Deshalb setzen wir selbst auf dieser Website z. B. bewusst keine eigenen  Cookies ein. Im Einklang mit der gesetzlichen Vorgabe zur Datensparsamkeit verarbeiten wir nur die nötigsten personenbezogenen Daten. Einzelheiten zu Art, Umfang und Dauer unserer Datenverarbeitung sowie Ihre Rechte als Betroffener finden Sie nachstehend. 

## Wer ist verantwortlich

Der Verantwortliche im Sinne der EU-Datenschutzgrundverordnung (DSGVO) und anderer nationaler Datenschutzgesetze der Mitgliedsstaaten sowie sonstiger datenschutzrechtlicher Bestimmungen ist:

```
Evangelische Gemeinde jesusfriends e.V.
Schäferkampsallee 36
20357 Hamburg 
Deutschland
E-Mail: contact@jesusfriends.de
Link zum Impressum: https://jesusfriends.de/impressum/
```

# Allgemeines zur Datenverarbeitung

## Umfang der Verarbeitung personenbezogener Daten
Wir verarbeiten personenbezogene Daten von Nutzern dieser Website grundsätzlich nur, soweit dies zur Bereitstellung einer funktionsfähigen Website sowie unserer Inhalte und Dienste erforderlich ist. Die Verarbeitung personenbezogener Daten erfolgt regelmäßig nur nach Einwilligung der betroffenen Person. Eine Ausnahme gilt in solchen Fällen, in denen es aus tatsächlichen Gründen nicht möglich ist eine solche Einwilligung vorher einzuholen und die Verarbeitung der Daten durch gesetzliche Vorschriften gestattet ist. 

## Rechtsgrundlage für die Verarbeitung personenbezogener Daten
Soweit wir für Verarbeitungsvorgänge personenbezogener Daten eine Einwilligung der betroffenen Person einholen, dient Art. 6 Abs. 1 lit. a DSGVO als Rechtsgrundlage.
Soweit eine Verarbeitung personenbezogener Daten zur Erfüllung einer rechtlichen Verpflichtung erforderlich ist, der unser Verein unterliegt, dient Art. 6 Abs. 1 lit. c DSGVO als Rechtsgrundlage.
Ist die Verarbeitung zur Wahrung eines berechtigten Interesses unseres Vereins oder eines Dritten erforderlich und überwiegen die Interessen, Grundrechte und Grundfreiheiten des Betroffenen das erstgenannte Interesse nicht, so dient Art. 6 Abs. 1 lit. f DSGVO als Rechtsgrundlage für die Verarbeitung. 

## Datenlöschung und Speicherdauer
Die personenbezogenen Daten der betroffenen Person werden gelöscht oder gesperrt, sobald der Zweck der Speicherung entfällt. Eine Speicherung kann darüber hinaus erfolgen, wenn dies durch den europäischen oder nationalen Gesetzgeber in unionsrechtlichen Verordnungen, Gesetzen oder sonstigen Vorschriften, denen der Verantwortliche unterliegt, vorgesehen wurde. Eine Sperrung oder Löschung der Daten erfolgt auch dann, wenn eine durch die genannten Normen vorgeschriebene Speicherfrist abläuft, es sei denn, dass eine Erforderlichkeit zur weiteren Speicherung der Daten für einen Vertragsabschluss oder eine Vertragserfüllung besteht.

# Bereitstellung der Website und Erstellung von Logfiles

## Beschreibung und Umfang der Datenverarbeitung
Um diese Website betreiben zu können, nutzen wir technische Infrastruktur und Dienstleistungen des Hostinganbieters y6 internetkommunikation (Link: https://www.y-6.de/). Bei jedem Aufruf unserer Internetseite erfasst das System unseres Hostinganbieters automatisiert Daten und Informationen vom Computersystem des aufrufenden Rechners. Die Erfassung erfolgt allerdings in anonymisierter Form. Dabei wird der letzte Teil der IP-Adresse des Nutzers ausgetauscht.  Auf diese Weise ist eine Zuordnung der IP-Adresse zum aufrufenden Rechner nicht mehr möglich. Die Statistiken basieren auf diesen anonymisierten Daten. Die Daten werden in den Logfiles auf dem System unseres Hostinganbieters gespeichert. Eine Speicherung dieser Daten zusammen mit anderen personenbezogenen Daten des Nutzers findet nicht statt. Der Vollständigkeit halber listen wir nachstehend dennoch auf, welche Daten beim Aufruf der Website erhoben werden:  
 
1. Name der aufgerufenen Website
2. Datum und Uhrzeit des Zugriffs
3. Übertragene Datenmenge
4. Meldung über den erfolgreichen Abruf
5. Informationen über den Browsertyp und die verwendete Version
6. Das Betriebssystem des aufrufenden Rechners
7. Den Internet-Service-Provider des aufrufenden Rechners
8. Die IP-Adresse des Nutzers in anonymisierter Form
9. Websites, von denen der aufrufende Rechner auf unsere Internetseite gelangt 



## Rechtsgrundlage für die Datenverarbeitung 
Rechtsgrundlage für die vorübergehende Speicherung der Daten und der Logfiles ist Art. 6 Abs. 1 lit. f DSGVO.

## Zweck der Datenverarbeitung
Die vorübergehende Speicherung der IP-Adresse durch das System ist notwendig, um eine Auslieferung der Website an den Rechner des Nutzers zu ermöglichen. Hierfür muss die IP-Adresse des Nutzers für die Dauer der Sitzung gespeichert bleiben. 

Die Speicherung in Logfiles erfolgt, um die Funktionsfähigkeit der Website sicherzustellen. Zudem dienen uns die Daten zur Optimierung der Website und zur Sicherstellung der Sicherheit unserer informationstechnischen Systeme, z.B. um Missbrauchs- und Betrugsfälle aufzuklären. Eine Auswertung der Daten zu Marketingzwecken findet nicht statt. 

In diesen Zwecken liegt auch unser berechtigtes Interesse an der Datenverarbeitung nach Art. 6 Abs. 1 lit. f DSGVO.

## Dauer der Speicherung
Die Logfiles mit den oben genannten Daten werden von unserem Provider nach einem halben Jahr gelösch. Darauf haben wir leider keinen Einfluss. Allerdings werden, wie gesagt, nur anonymisierte Daten in den Logfiles gespeichert. Die Erfassung der Daten zur Bereitstellung der Website und die Speicherung der Daten in Logfiles ist für den Betrieb der Internetseite zwingend erforderlich. Es besteht folglich seitens des Nutzers keine Widerspruchsmöglichkeit. 

# Newsletter

## Beschreibung und Umfang der Datenverarbeitung
Auf unserer Internetseite besteht die Möglichkeit einen kostenfreien Newsletter zu abonnieren. Dabei werden bei der Anmeldung zum Newsletter die Daten aus der Eingabemaske (Vorname, Name und E-Mail-Adresse) an uns übermittelt.

Zudem werden folgende Daten bei der Anmeldung erhoben:

1. IP-Adresse des aufrufenden Rechners
2. Datum und Uhrzeit der Registrierung

Für die Verarbeitung der Daten wird im Rahmen des Anmeldevorgangs Ihre Einwilligung eingeholt und auf diese Datenschutzerklärung verwiesen.

Es erfolgt im Zusammenhang mit der Datenverarbeitung für den Versand von Newslettern keine Weitergabe der Daten an Dritte. Die Daten werden ausschließlich für den Versand des Newsletters verwendet.
## Rechtsgrundlage für die Datenverarbeitung
Rechtsgrundlage für die Verarbeitung der Daten nach Anmeldung zum Newsletters durch den Nutzer ist bei Vorliegen einer Einwilligung des Nutzers Art. 6 Abs. 1 lit. a DSGVO.

## Zweck der Datenverarbeitung
Die Erhebung der E-Mail-Adresse des Nutzers dient dazu, den Newsletter zuzustellen. 

Die Erhebung sonstiger personenbezogener Daten im Rahmen des Anmeldevorgangs dient dazu, einen Missbrauch der Dienste oder der verwendeten E-Mail-Adresse zu verhindern.

## Dauer der Speicherung
Die Daten werden gelöscht, sobald sie für die Erreichung des Zweckes ihrer Erhebung nicht mehr erforderlich sind. Die Daten des Nutzers werden demnach solange gespeichert, wie das Abonnement des Newsletters aktiv ist. 


## Widerspruchs- und Beseitigungsmöglichkeit
Das Abonnement des Newsletters kann durch den betroffenen Nutzer jederzeit gekündigt werden. Zu diesem Zweck findet sich in jedem Newsletter ein entsprechender Link. 

Hierdurch wird ebenfalls ein Widerruf der Einwilligung der Speicherung der während des Anmeldevorgangs erhobenen personenbezogenen Daten ermöglicht.

# Kontaktformular und E-Mail-Kontakt

## Beschreibung und Umfang der Datenverarbeitung
Auf unserer Internetseite ist ein Kontaktformular vorhanden, welches für die elektronische Kontaktaufnahme genutzt werden kann. Nimmt ein Nutzer diese Möglichkeit wahr, so werden die in der Eingabemaske eingegeben Daten  an uns übermittelt und gespeichert. 

Für die Verarbeitung der Daten wird im Rahmen des Absendevorgangs Ihre Einwilligung eingeholt und auf diese Datenschutzerklärung verwiesen.

Alternativ ist eine Kontaktaufnahme über die bereitgestellte E-Mail-Adresse möglich. In diesem Fall werden die mit der E-Mail übermittelten personenbezogenen Daten des Nutzers gespeichert. 

Es erfolgt in diesem Zusammenhang keine Weitergabe der Daten an Dritte. Die Daten werden ausschließlich für die Verarbeitung der Konversation verwendet.

## Rechtsgrundlage für die Datenverarbeitung 
Rechtsgrundlage für die Verarbeitung der Daten ist bei Vorliegen einer Einwilligung des Nutzers Art. 6 Abs. 1 lit. a DSGVO.

Rechtsgrundlage für die Verarbeitung der Daten, die im Zuge einer Übersendung einer E-Mail übermittelt werden, ist Art. 6 Abs. 1 lit. f DSGVO. 

## Zweck der Datenverarbeitung
Die Verarbeitung der personenbezogenen Daten aus der Eingabemaske dient uns allein zur Bearbeitung der Kontaktaufnahme. Im Falle einer Kontaktaufnahme per E-Mail liegt hieran auch das erforderliche berechtigte Interesse an der Verarbeitung der Daten.


## Dauer der Speicherung
Die Daten werden gelöscht, sobald sie für die Erreichung des Zweckes ihrer Erhebung nicht mehr erforderlich sind. Dies ist dann der Fall, wenn die jeweilige Konversation mit dem Nutzer beendet ist. Beendet ist die Konversation dann, wenn sich aus den Umständen entnehmen lässt, dass der betroffene Sachverhalt abschließend geklärt ist. 



## Widerspruchs- und Beseitigungsmöglichkeit
Der Nutzer hat jederzeit die Möglichkeit, seine Einwilligung zur Verarbeitung der personenbezogenen Daten zu widerrufen. Nimmt der Nutzer per E-Mail Kontakt mit uns auf, so kann er der Speicherung seiner personenbezogenen Daten jederzeit widersprechen. Der Widerspruch kann über die im Impressum angegebenen Kontaktmöglichkeiten erklärt werden. In Fall eines Widerspruchs kann die Konversation nicht fortgeführt werden. Alle personenbezogenen Daten, die im Zuge der Kontaktaufnahme gespeichert wurden, werden in diesem Fall gelöscht.

# Webanalyse durch Matomo (ehemals PIWIK)

## Umfang der Verarbeitung personenbezogener Daten
Wir nutzen auf unserer Website das Open-Source-Software-Tool Matomo (ehemals PIWIK) zur Analyse des Surfverhaltens auf unserer Website.  Werden Einzelseiten unserer Website aufgerufen, so werden folgende Daten gespeichert:

1. Die aufgerufene Webseite
2. Zwei Bytes der IP-Adresse des aufrufenden Rechnerns
3. Informationen über den Browsertyp und die verwendete Version
4. Das Betriebssystem des aufrufenden Rechners 
5. Herkunftsland, Datum und Uhrzeit der Serveranfrage
6. Die Website, von der der aufrufende Rechner auf die aufgerufene Webseite gelangt ist (Referrer)
7. Die Unterseiten, die von der aufgerufenen Webseite aus aufgerufen werden
8. Die Verweildauer auf der Webseite

Die Software läuft dabei ausschließlich auf den Servern unserer Webseite. Eine Speicherung der oben genannten Daten findet nur dort statt. Eine Weitergabe der Daten an Dritte erfolgt nicht. 

Die Software ist so eingestellt, dass die IP-Adressen nicht vollständig gespeichert werden, sondern 2 Bytes der IP-Adresse maskiert werden (Bsp.:  192.168.xxx.xxx). Auf diese Weise ist eine Zuordnung der gekürzten IP-Adresse zum aufrufenden Rechner nicht mehr möglich.

## Rechtsgrundlage für die Verarbeitung personenbezogener Daten
Rechtsgrundlage für die Verarbeitung der oben genannten Daten ist Art. 6 Abs. 1 lit. f DSGVO.

## Zweck der Datenverarbeitung
Die Verarbeitung der oben genannten Daten  ermöglicht uns eine Analyse des Surfverhaltens auf unserer Website. Wir sind  durch die Auswertung der gewonnen Daten in der Lage, Informationen über die Nutzung der einzelnen Komponenten unserer Webseite zusammenzustellen. Dies hilft uns dabei unsere Webseite und deren Nutzerfreundlichkeit stetig zu verbessern. In diesen Zwecken liegt auch unser berechtigtes Interesse in der Verarbeitung der Daten nach Art. 6 Abs. 1 lit. f DSGVO. Durch die Anonymisierung der IP-Adresse wird dem Interesse der Nutzer an deren Schutz personenbezogener Daten hinreichend Rechnung getragen.

## Dauer der Speicherung
Die Daten werden gelöscht, sobald sie für unsere Aufzeichnungszwecke nicht mehr benötigt werden. In unserem Fall ist dies nach 180 Tagen der Fall.

## Widerspruchs- und Beseitigungsmöglichkeit


Wir bieten unseren Nutzern auf unserer Website die Möglichkeit eines Opt-Out aus dem Analyseverfahren. Hierzu müssen Sie dem entsprechenden Link folgen. Auf diese Weise wird ein Cookie auf Ihrem System gesetzt, der unserem System signalisiert die Daten des Nutzers nicht zu speichern. Löscht der Nutzer den entsprechenden Cookie zwischenzeitlich vom eigenen System, so muss er den Opt-Out-Cookie erneut setzten.
Des weiteren können Sie in Ihrem Browser die Do-Not-Track-Einstellung aktivieren. Dadurch können Sie auch jedes Tracking durch Matomo verhindern, das nicht auf dem Setzen von Cookies basiert. 
Nähere Informationen zu den Privatsphäreeinstellungen der Matomo Software finden Sie unter folgendem Link: https://matomo.org/docs/privacy/.

# Einbindung von Diensten und Inhalten Dritter
Wir setzen auf unserer Website Inhalte und Serviceangebote von Drittanbietern ein, z.B. um Videos oder Schriftarten einzubinden. Dies setzt immer voraus, dass die Drittanbieter, die IP-Adresse der Nutzer wahrnehmen, da sie sonst keine Inhalte an deren Browser senden könnten. Die IP-Adresse ist damit für die Darstellung dieser Inhalte erforderlich. Wir bemühen uns nur solche Inhalte zu verwenden, deren jeweilige Anbieter die IP-Adresse lediglich zur Auslieferung der Inhalte verwenden. 

Drittanbieter können ferner so genannte Pixel-Tags (unsichtbare Grafiken, auch als “Web Beacons” bezeichnet) für statistische oder Marketingzwecke verwenden. Durch die “Pixel-Tags” können Informationen, wie der Besucherverkehr auf den Seiten dieser Website ausgewertet werden. Die pseudonymen Informationen können ferner in Cookies auf dem Gerät der Nutzer gespeichert werden und unter anderem technische Informationen zum Browser und Betriebssystem, verweisende Webseiten, Besuchszeit sowie weitere Angaben zur Nutzung unseres Onlineangebotes enthalten, als auch mit solchen Informationen aus anderen Quellen verbunden werden. Sie können das Setzen solcher Drittanbieter-Cookies verhindern, in dem Sie in Ihrem Browser Drittanbieter-Cookies deaktivieren. 

Wir verwenden Inhalte und Services der folgenden Anbieter: 

## Youtube
Wir binden die Videos der Plattform “YouTube” des Anbieters Google LLC, 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA, ein. 
Datenschutzerklärung: https://www.google.com/policies/privacy/, 
Opt-Out: https://adssettings.google.com/authenticated.

## Google Fonts
Wir binden die Schriftarten (“Google Fonts”) des Anbieters Google LLC, 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA, ein. 
Datenschutzerklärung: https://www.google.com/policies/privacy/, 
Opt-Out: https://adssettings.google.com/authenticated. 

## Podlove
Podlove Publisher erfasst Download-Statistiken für die auf unserer Website angebotenen Podcasts. 

IP Adressen werden benutzt, um eine geschätzte geographische Verortung (Stadt / Land) durchzuführen. Hierfür werden IP Adressen kurzzeitig (bis zu 48 Stunden) als Teil einer „request id” gespeichert. Das ist notwendig, damit der Podcastanbieter die Tragfähigkeit seiner Bemühungen nachweisen kann.

Um realistische Download-Zahlen zu ermitteln, muss das System erneuten Zugriff auf die gleiche Datei vom selben Nutzer erkennen können. Der einzige verlässliche Weg dies zu erreichen ist, die IP zusammen mit dem User Agent zu betrachten. Eine anonymisierte IP zu benutzen ist nicht möglich, da dies zu falschen Ergebnissen führen würde. Ein Zugriff auf die gleiche Datei vom selben Nutzer an verschiedenen Tagen kann als separater Download betrachtet werden. Darum genügt es, IPs lediglich für bis zu 48 Stunden vorzuhalten.

Nach 48 Stunden werden „request ids” „salted”, was es unmöglich macht, die ursprüngliche darin enthaltene IP wiederherzustellen.

Der Browser „User Agent” wird ebenso gespeichert.

# Rechte der betroffenen Person
Werden personenbezogene Daten von Ihnen verarbeitet, sind Sie Betroffener i.S.d. DSGVO und es stehen Ihnen folgende Rechte gegenüber dem Verantwortlichen zu:

## Auskunftsrecht
Sie können von dem Verantwortlichen eine Bestätigung darüber verlangen, ob personenbezogene Daten, die Sie betreffen, von uns verarbeitet werden. 
Liegt eine solche Verarbeitung vor, können Sie von dem Verantwortlichen über folgende Informationen Auskunft verlangen:

1.	die Zwecke, zu denen die personenbezogenen Daten verarbeitet werden;
2.	die Kategorien von personenbezogenen Daten, welche verarbeitet werden;
3.	die Empfänger bzw. die Kategorien von Empfängern, gegenüber denen die Sie betreffenden personenbezogenen Daten offengelegt wurden oder noch offengelegt werden;
4.	die geplante Dauer der Speicherung der Sie betreffenden personenbezogenen Daten oder, falls konkrete Angaben hierzu nicht möglich sind, Kriterien für die Festlegung der Speicherdauer;
5.	das Bestehen eines Rechts auf Berichtigung oder Löschung der Sie betreffenden personenbezogenen Daten, eines Rechts auf Einschränkung der Verarbeitung durch den Verantwortlichen oder eines Widerspruchsrechts gegen diese Verarbeitung; 
6.	das Bestehen eines Beschwerderechts bei einer Aufsichtsbehörde;
7.	alle verfügbaren Informationen über die Herkunft der Daten, wenn die personenbezogenen Daten nicht bei der betroffenen Person erhoben werden;
8.	das Bestehen einer automatisierten Entscheidungsfindung einschließlich Profiling gemäß Art. 22 Abs. 1 und 4 DSGVO und – zumindest in diesen Fällen – aussagekräftige Informationen über die involvierte Logik sowie die Tragweite und die angestrebten Auswirkungen einer derartigen Verarbeitung für die betroffene Person.

Ihnen steht das Recht zu, Auskunft darüber zu verlangen, ob die Sie betreffenden personenbezogenen Daten in ein Drittland oder an eine internationale Organisation übermittelt werden. In diesem Zusammenhang können Sie verlangen, über die geeigneten Garantien gem. Art. 46 DSGVO im Zusammenhang mit der Übermittlung unterrichtet zu werden.

## Recht auf Berichtigung 
Sie haben ein Recht auf Berichtigung und/oder Vervollständigung gegenüber dem Verantwortlichen, sofern die verarbeiteten personenbezogenen Daten, die Sie betreffen, unrichtig oder unvollständig sind. Der Verantwortliche hat die Berichtigung unverzüglich vorzunehmen.

## Recht auf Einschränkung der Verarbeitung
Unter den folgenden Voraussetzungen können Sie die Einschränkung der Verarbeitung der Sie betreffenden personenbezogenen Daten verlangen:

1. wenn Sie die Richtigkeit der Sie betreffenden personenbezogenen für eine Dauer bestreiten, die es dem Verantwortlichen ermöglicht, die Richtigkeit der personenbezogenen Daten zu überprüfen;
2. die Verarbeitung unrechtmäßig ist und Sie die Löschung der personenbezogenen Daten ablehnen und stattdessen die Einschränkung der Nutzung der personenbezogenen Daten verlangen;
3. der Verantwortliche die personenbezogenen Daten für die Zwecke der Verarbeitung nicht länger benötigt, Sie diese jedoch zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen benötigen, oder
4. wenn Sie Widerspruch gegen die Verarbeitung gemäß Art. 21 Abs. 1 DSGVO eingelegt haben und noch nicht feststeht, ob die berechtigten Gründe des Verantwortlichen gegenüber Ihren Gründen überwiegen.

Wurde die Verarbeitung der Sie betreffenden personenbezogenen Daten eingeschränkt, dürfen diese Daten – von ihrer Speicherung abgesehen – nur mit Ihrer Einwilligung oder zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen oder zum Schutz der Rechte einer anderen natürlichen oder juristischen Person oder aus Gründen eines wichtigen öffentlichen Interesses der Union oder eines Mitgliedstaats verarbeitet werden.
Wurde die Einschränkung der Verarbeitung nach den o.g. Voraussetzungen eingeschränkt, werden Sie von dem Verantwortlichen unterrichtet bevor die Einschränkung aufgehoben wird.

## Recht auf Löschung
a) Löschungspflicht

Sie können von dem Verantwortlichen verlangen, dass die Sie betreffenden personenbezogenen Daten unverzüglich gelöscht werden, und der Verantwortliche ist verpflichtet, diese Daten unverzüglich zu löschen, sofern einer der folgenden Gründe zutrifft:

(1)	Die Sie betreffenden personenbezogenen Daten sind für die Zwecke, für die sie erhoben oder auf sonstige Weise verarbeitet wurden, nicht mehr notwendig.
(2)	Sie widerrufen Ihre Einwilligung, auf die sich die Verarbeitung gem. Art. 6 Abs. 1 lit. a oder Art. 9 Abs. 2 lit. a DSGVO stützte, und es fehlt an einer anderweitigen Rechtsgrundlage für die Verarbeitung. 
(3)	Sie legen gem. Art. 21 Abs. 1 DSGVO Widerspruch gegen die Verarbeitung ein und es liegen keine vorrangigen berechtigten Gründe für die Verarbeitung vor, oder Sie legen gem. Art. 21 Abs. 2 DSGVO Widerspruch gegen die Verarbeitung ein. 
(4)	Die Sie betreffenden personenbezogenen Daten wurden unrechtmäßig verarbeitet. 
(5)	Die Löschung der Sie betreffenden personenbezogenen Daten ist zur Erfüllung einer rechtlichen Verpflichtung nach dem Unionsrecht oder dem Recht der Mitgliedstaaten erforderlich, dem der Verantwortliche unterliegt. 
(6)		Die Sie betreffenden personenbezogenen Daten wurden in Bezug auf angebotene Dienste der Informationsgesellschaft gemäß Art. 8 Abs. 1 DSGVO erhoben.

a) Information an Dritte
Hat der Verantwortliche die Sie betreffenden personenbezogenen Daten öffentlich gemacht und ist er gem. Art. 17 Abs. 1 DSGVO zu deren Löschung verpflichtet, so trifft er unter Berücksichtigung der verfügbaren Technologie und der Implementierungskosten angemessene Maßnahmen, auch technischer Art, um für die Datenverarbeitung Verantwortliche, die die personenbezogenen Daten verarbeiten, darüber zu informieren, dass Sie als betroffene Person von ihnen die Löschung aller Links zu diesen personenbezogenen Daten oder von Kopien oder Replikationen dieser personenbezogenen Daten verlangt haben. 

b) Ausnahmen
Das Recht auf Löschung besteht nicht, soweit die Verarbeitung erforderlich ist

(1)	zur Ausübung des Rechts auf freie Meinungsäußerung und Information;
(2)	zur Erfüllung einer rechtlichen Verpflichtung, die die Verarbeitung nach dem Recht der Union oder der Mitgliedstaaten, dem der Verantwortliche unterliegt, erfordert, oder zur Wahrnehmung einer Aufgabe, die im öffentlichen Interesse liegt oder in Ausübung öffentlicher Gewalt erfolgt, die dem Verantwortlichen übertragen wurde;
(3)	aus Gründen des öffentlichen Interesses im Bereich der öffentlichen Gesundheit gemäß Art. 9 Abs. 2 lit. h und i sowie Art. 9 Abs. 3 DSGVO;
(4)	für im öffentlichen Interesse liegende Archivzwecke, wissenschaftliche oder historische Forschungszwecke oder für statistische Zwecke gem. Art. 89 Abs. 1 DSGVO, soweit das unter Abschnitt a) genannte Recht voraussichtlich die Verwirklichung der Ziele dieser Verarbeitung unmöglich macht oder ernsthaft beeinträchtigt, oder
(5)	zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen.


## Recht auf Unterrichtung
Haben Sie das Recht auf Berichtigung, Löschung oder Einschränkung der Verarbeitung gegenüber dem Verantwortlichen geltend gemacht, ist dieser verpflichtet, allen Empfängern, denen die Sie betreffenden personenbezogenen Daten offengelegt wurden, diese Berichtigung oder Löschung der Daten oder Einschränkung der Verarbeitung mitzuteilen, es sei denn, dies erweist sich als unmöglich oder ist mit einem unverhältnismäßigen Aufwand verbunden.
Ihnen steht gegenüber dem Verantwortlichen das Recht zu, über diese Empfänger unterrichtet zu werden.

## Recht auf Datenübertragbarkeit
Sie haben das Recht, die Sie betreffenden personenbezogenen Daten, die Sie dem Verantwortlichen bereitgestellt haben, in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten. Außerdem haben Sie das Recht diese Daten einem anderen Verantwortlichen ohne Behinderung durch den Verantwortlichen, dem die personenbezogenen Daten bereitgestellt wurden, zu übermitteln, sofern

(1)	die Verarbeitung auf einer Einwilligung gem. Art. 6 Abs. 1 lit. a DSGVO oder Art. 9 Abs. 2 lit. a DSGVO oder auf einem Vertrag gem. Art. 6 Abs. 1 lit. b DSGVO beruht und
(2)	die Verarbeitung mithilfe automatisierter Verfahren erfolgt.

In Ausübung dieses Rechts haben Sie ferner das Recht, zu erwirken, dass die Sie betreffenden personenbezogenen Daten direkt von einem Verantwortlichen einem anderen Verantwortlichen übermittelt werden, soweit dies technisch machbar ist. Freiheiten und Rechte anderer Personen dürfen hierdurch nicht beeinträchtigt werden.
Das Recht auf Datenübertragbarkeit gilt nicht für eine Verarbeitung personenbezogener Daten, die für die Wahrnehmung einer Aufgabe erforderlich ist, die im öffentlichen Interesse liegt oder in Ausübung öffentlicher Gewalt erfolgt, die dem Verantwortlichen übertragen wurde.

## Widerspruchsrecht
Sie haben das Recht, aus Gründen, die sich aus ihrer besonderen Situation ergeben, jederzeit gegen die Verarbeitung der Sie betreffenden personenbezogenen Daten, die aufgrund von Art. 6 Abs. 1 lit. e oder f DSGVO erfolgt, Widerspruch einzulegen; dies gilt auch für ein auf diese Bestimmungen gestütztes Profiling. 
Der Verantwortliche verarbeitet die Sie betreffenden personenbezogenen Daten nicht mehr, es sei denn, er kann zwingende schutzwürdige Gründe für die Verarbeitung nachweisen, die Ihre Interessen, Rechte und Freiheiten überwiegen, oder die Verarbeitung dient der Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen.
Werden die Sie betreffenden personenbezogenen Daten verarbeitet, um Direktwerbung zu betreiben, haben Sie das Recht, jederzeit Widerspruch gegen die Verarbeitung der Sie betreffenden personenbezogenen Daten zum Zwecke derartiger Werbung einzulegen; dies gilt auch für das Profiling, soweit es mit solcher Direktwerbung in Verbindung steht.
Widersprechen Sie der Verarbeitung für Zwecke der Direktwerbung, so werden die Sie betreffenden personenbezogenen Daten nicht mehr für diese Zwecke verarbeitet.
Sie haben die Möglichkeit, im Zusammenhang mit der Nutzung von Diensten der Informationsgesellschaft – ungeachtet der Richtlinie 2002/58/EG – Ihr Widerspruchsrecht mittels automatisierter Verfahren auszuüben, bei denen technische Spezifikationen verwendet werden.

## Recht auf Widerruf der datenschutzrechtlichen Einwilligungserklärung
Sie haben das Recht, Ihre datenschutzrechtliche Einwilligungserklärung jederzeit zu widerrufen. Durch den Widerruf der Einwilligung wird die Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten Verarbeitung nicht berührt.

## Recht auf Beschwerde bei einer Aufsichtsbehörde
Unbeschadet eines anderweitigen verwaltungsrechtlichen oder gerichtlichen Rechtsbehelfs steht Ihnen das Recht auf Beschwerde bei einer Aufsichtsbehörde, insbesondere in dem Mitgliedstaat ihres Aufenthaltsorts, ihres Arbeitsplatzes oder des Orts des mutmaßlichen Verstoßes, zu, wenn Sie der Ansicht sind, dass die Verarbeitung der Sie betreffenden personenbezogenen Daten gegen die DSGVO verstößt. 
Die Aufsichtsbehörde, bei der die Beschwerde eingereicht wurde, unterrichtet den Beschwerdeführer über den Stand und die Ergebnisse der Beschwerde einschließlich der Möglichkeit eines gerichtlichen Rechtsbehelfs nach Art. 78 DSGVO.
